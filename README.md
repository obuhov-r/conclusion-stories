# conclusion-stories

## Sorry, russian language only

Проект просто для прикола :)

Немного неофициальной документации и как-замутить, связанные с SoftShell/Conclusion от Eller (http://eller.cz/conclusion.php)

Запущено здесь - https://obuhov-r.gitlab.io/conclusion-stories

Отчеты сборки:

* для современных браузеров - https://obuhov-r.gitlab.io/conclusion-stories/report.html
* для старых браузеров - https://obuhov-r.gitlab.io/conclusion-stories/legacy-report.html
