export interface IArticleInfo {
    path: string;
    title: string;
    tags: Array<string>;
    children: Array<IArticleInfo>;
}