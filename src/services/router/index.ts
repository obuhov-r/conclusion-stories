import Vue from 'vue';
import VueRouter from 'vue-router';

import paths from './paths';

Vue.use(VueRouter);

const routes = paths;

const router = new VueRouter({
    routes,
});

export default router;