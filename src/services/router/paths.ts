import { RouteConfig } from 'vue-router';


interface IPath extends RouteConfig {
    tags?: Array<string>;
}


const paths: Array<IPath> = [
    {
        path: '/',
        component: () => {
            // eslint-disable-next-line no-inline-comments
            return import(/* webpackChunkName: "about" */ '@/views/Home.vue');
        },
    },
    {
        path: '/about',

        /* route level code-splitting
           this generates a separate chunk (about.[hash].js) for this route
           which is lazy-loaded when the route is visited. */
        component: () => {
            // eslint-disable-next-line no-inline-comments
            return import(/* webpackChunkName: "about" */ '@/views/About.vue');
        },
    },
    {
        path: '*',
        component: () => {
            // eslint-disable-next-line no-inline-comments
            return import(/* webpackChunkName: "not-found" */ '@/views/not-found');
        },
    },
];

export default paths;