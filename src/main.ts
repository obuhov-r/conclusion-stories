import Vue from 'vue';
import components from '@/components';
import App from '@/app';
import router from './services/router';
import '@/style/imports.scss';
import '@/style/main.scss';


Vue.config.productionTip = false;

Vue.use(components);

new Vue({
    router,
    render: (h) => {
        return h(App);
    },
}).$mount('#app');