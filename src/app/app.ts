import { Component, Vue } from 'vue-property-decorator';

import { lorem } from '@/utils';


@Component
export default class App extends Vue {
    private lorem = lorem;
    private sideBarVisible = false;


    // #region Lifecycle
    private created(): void {
        this.$watch('scrollTop', this.scrollTopChanged);

        window.addEventListener('resize', this.onWindowResize);
    }

    private mounted(): void {
        this.updateWindowHeight();
        this.updateNavbarHeight();

        const el = this.$el;
        if (el instanceof HTMLElement) {
            this.updateScrollTop(el);
        }
    }

    private beforeDestroy(): void {
        window.removeEventListener('resize', this.onWindowResize);
    }
    // #endregion


    // #region Scroll
    private scrolledTop = true;
    private scrollTop = 0;
    private scrollUpdateHandle: number | undefined;

    private scrollTopChanged(n: number, o: number): void {
        const scrolledTop = (n < o);
        if (this.scrolledTop !== scrolledTop) {
            this.scrolledTop = scrolledTop;
        }
    }

    private updateScrollTop(source: HTMLElement): void {
        const scrollTop = source.scrollTop;
        if (this.scrollTop !== scrollTop) {
            this.scrollTop = scrollTop;

            const scrolledTop = (scrollTop < this.scrollTop);
            if (this.scrolledTop !== scrolledTop) {
                this.scrolledTop = scrolledTop;
            }
        }
    }

    private scheduleScrollTopUpdate(source: HTMLElement): void {
        if (this.scrollUpdateHandle !== undefined) {
            clearTimeout(this.scrollUpdateHandle);
        }

        this.scrollUpdateHandle = setTimeout(() => {
            this.updateScrollTop(source);
        }, 100);
    }

    private onScroll(ev: UIEvent): void {
        const target = ev.target;
        if (target instanceof HTMLElement) {
            this.updateScrollTop(target);
        }
    }
    // #endregion


    // #region Navbar
    private navbarHeight = 0;

    private get navbarVisible(): boolean {
        return (this.windowHeight > 600) || (this.scrolledTop);
    }

    private getNavbar(): HTMLElement | undefined {
        const result = this.$refs.navbar;
        if (result instanceof HTMLElement) {
            return result;
        }

        return undefined;
    }

    private updateNavbarHeight(): void {
        let navbarHeight = 0;

        const navbar = this.getNavbar();
        if (navbar !== undefined) {
            navbarHeight = navbar.offsetHeight;
        }

        if (this.navbarHeight !== navbarHeight) {
            this.navbarHeight = navbarHeight;
        }
    }
    // #endregion


    // #region Window resize
    private windowHeight = 0;

    private updateWindowHeight(): void {
        const windowHeight = window.innerHeight;
        if (this.windowHeight !== windowHeight) {
            this.windowHeight = windowHeight;
        }
    }

    private onWindowResize(): void {
        this.updateWindowHeight();
        this.updateNavbarHeight();
    }
    // #endregion
}