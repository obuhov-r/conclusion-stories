import { FunctionalComponentOptions, VNode } from 'vue';

import './c-icon.scss';
import variants from './variants';


const options: FunctionalComponentOptions = {
    functional: true,

    render: (createElement, context): VNode | Array<VNode> => {
        let variantName = '';
        const slots = context.slots();
        if ((typeof slots === 'object') && (slots !== null)) {
            const defaultSlot = slots.default;
            if (Array.isArray(defaultSlot)) {
                defaultSlot.forEach((slotItem) => {
                    if ((typeof slotItem === 'object') && (slotItem !== null)) {
                        const itemText = slotItem.text;
                        if (typeof itemText === 'string') {
                            variantName += itemText.trim();
                        }
                    }
                });
            }
        }

        const variantValue = (variantName.length === 0 ? undefined : variants[variantName]);
        if (variantValue === undefined) {
            return [];
        }

        const path = createElement(
            'path',
            {
                attrs: {
                    d: variantValue,
                },
            },
        );

        if (typeof context.data.staticClass !== 'string') {
            context.data.staticClass = '';
        }
        context.data.staticClass = `${context.data.staticClass.trim()} c-icon`.trim();

        if ((typeof context.data.attrs !== 'object') || (context.data.attrs === null)) {
            context.data.attrs = {};
        }
        context.data.attrs.viewBox = '0 0 24 24';

        return createElement(
            'svg',
            context.data,
            [ path ],
        );
    },
};
export default options;