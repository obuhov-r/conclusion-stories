import {
    BButton,
    BCollapse, BContainer,
    BFormInput,
    BInputGroup,
    BLink,
    BNavForm, BNavItem, BNavbar, BNavbarBrand, BNavbarNav, BNavbarToggle,
    BOverlay,
    BSidebar,
} from 'bootstrap-vue';
import Vue, { PluginFunction, VueConstructor } from 'vue';

import cIconOptions from './c-icon';


const pluginFunction: PluginFunction<unknown> = (vue: VueConstructor<Vue>) => {
    vue.component('b-button', BButton);
    vue.component('b-collapse', BCollapse);
    vue.component('b-container', BContainer);
    vue.component('b-form-input', BFormInput);
    vue.component('b-input-group', BInputGroup);
    vue.component('b-link', BLink);
    vue.component('b-nav-form', BNavForm);
    vue.component('b-nav-item', BNavItem);
    vue.component('b-navbar', BNavbar);
    vue.component('b-navbar-brand', BNavbarBrand);
    vue.component('b-navbar-nav', BNavbarNav);
    vue.component('b-navbar-toggle', BNavbarToggle);
    vue.component('b-overlay', BOverlay);
    vue.component('b-sidebar', BSidebar);

    vue.component<unknown>('c-icon', cIconOptions);
};
export default pluginFunction;