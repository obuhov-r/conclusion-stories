// https://eslint.org/docs/rules/#possible-errors
const possibleErrors = {
    'for-direction': 'error',
    'getter-return': 'error',
    'no-async-promise-executor': 'error',
    'no-await-in-loop': 'error',
    'no-compare-neg-zero': 'warn',
    'no-cond-assign': 'error',
    'no-console': [
        'warn',
        {
            'allow': ['warn', 'error']
        }
    ],
    'no-constant-condition': 'warn',
    'no-control-regex': 'error',
    'no-debugger': 'warn',
    'no-dupe-args': 'error',

    // Definition for rule 'no-dupe-else-if' was not found
    // 'no-dupe-else-if': 'error',

    'no-dupe-keys': 'error',
    'no-duplicate-case': 'error',
    'no-empty': 'warn',
    'no-empty-character-class': 'error',
    'no-ex-assign': 'error',
    'no-extra-boolean-cast': 'warn',
    'no-extra-parens': 'off',
    'no-extra-semi': 'warn',
    'no-func-assign': 'warn',

    // Definition for rule 'no-import-assign' was not found
    // 'no-import-assign': 'error',

    'no-inner-declarations': ['error', 'both'],
    'no-invalid-regexp': 'error',
    'no-irregular-whitespace': 'error',
    'no-misleading-character-class': 'error',
    'no-obj-calls': 'error',
    'no-prototype-builtins': 'error',
    'no-regex-spaces': 'warn',

    // Definition for rule 'no-setter-return' was not found
    // 'no-setter-return': 'error',

    'no-sparse-arrays': 'error',
    'no-template-curly-in-string': 'warn',
    'no-unexpected-multiline': 'error',
    'no-unreachable': 'warn',
    'no-unsafe-finally': 'error',
    'no-unsafe-negation': 'error',
    'require-atomic-updates': 'error',
    'use-isnan': 'error',
    'valid-typeof': 'error'
};

// https://eslint.org/docs/rules/#best-practices
const bestPractices = {
    'accessor-pairs': [
        'error',
        {
            // Error - should NOT have additional properties
            // 'enforceForClassMembers': true,
            'setWithoutGet': true
        }
    ],
    'array-callback-return': 'error',
    'block-scoped-var': 'error',
    'class-methods-use-this': 'off',
    'complexity': ['warn', 20],
    'consistent-return': 'off',
    'curly': ['warn', 'multi-line'],
    'default-case': 'warn',

    // Definition for rule 'default-param-last' was not found
    // 'default-param-last': 'warn',

    'dot-location': ['warn', 'property'],
    'dot-notation': 'warn',
    'eqeqeq': 'error',

    // Definition for rule 'grouped-accessor-pairs' was not found
    // 'grouped-accessor-pairs': [ 'error', 'getBeforeSet' ],

    'guard-for-in': 'warn',
    'max-classes-per-file': ['warn', 1],
    'no-alert': 'error',
    'no-caller': 'error',
    'no-case-declarations': 'error',

    // Definition for rule 'no-constructor-return' was not found
    // 'no-constructor-return': 'error',

    'no-div-regex': 'warn',
    'no-else-return': 'off',
    'no-empty-function': 'warn',
    'no-empty-pattern': 'warn',
    'no-eq-null': 'error',
    'no-eval': 'error',
    'no-extend-native': 'error',
    'no-extra-bind': 'warn',
    'no-extra-label': 'warn',
    'no-fallthrough': [
        'warn',
        {
            'commentPattern': 'plus[\\s\\w]*next[\\s\\w]*case'
        }
    ],
    'no-floating-decimal': 'warn',
    'no-global-assign': 'error',
    'no-implicit-coercion': 'error',
    'no-implicit-globals': 'error',
    'no-implied-eval': 'error',
    'no-invalid-this': 'error',
    'no-iterator': 'error',
    'no-labels': 'error',
    'no-lone-blocks': 'warn',
    'no-loop-func': 'warn',
    'no-magic-numbers': 'off',
    'no-multi-spaces': [
        'warn',
        {
            'ignoreEOLComments': true,
            'exceptions': {
                'Property': true,
                'VariableDeclarator': true,
                'ImportDeclaration': true
            }
        }
    ],
    'no-multi-str': 'error',
    'no-new': 'error',
    'no-new-func': 'error',
    'no-new-wrappers': 'error',
    'no-octal': 'error',
    'no-octal-escape': 'error',
    'no-param-reassign': 'error',
    'no-proto': 'error',
    'no-redeclare': 'error',
    'no-return-assign': ['error', 'always'],
    'no-return-await': 'error',
    'no-script-url': 'warn',
    'no-self-assign': 'error',
    'no-self-compare': 'error',
    'no-sequences': 'error',
    'no-throw-literal': 'error',
    'no-unmodified-loop-condition': 'warn',
    'no-unused-expressions': 'warn',
    'no-unused-labels': 'warn',
    'no-useless-call': 'warn',
    'no-useless-catch': 'warn',
    'no-useless-concat': 'warn',
    'no-useless-escape': 'warn',
    'no-useless-return': 'warn',
    'no-void': 'error',
    'no-warning-comments': [
        'warn',
        {
            'location': 'start',
            'terms': [
                'todo',
                'fixme'
            ]
        }
    ],
    'no-with': 'error',
    'prefer-named-capture-group': 'warn',
    'prefer-promise-reject-errors': 'error',

    // Definition for rule 'prefer-regex-literals' was not found
    // 'prefer-regex-literals': 'warn',

    'radix': 'error',
    'require-await': 'error',
    'require-unicode-regexp': 'error',
    'vars-on-top': 'warn',
    'wrap-iife': ['error', 'inside'],
    'yoda': [
        'warn',
        'never',
        {
            'exceptRange': true
        }
    ],
};

// https://eslint.org/docs/rules/#variables
const variables = {
    'init-declarations': 'off',
    'no-delete-var': 'error',
    'no-label-var': 'error',
    'no-restricted-globals': ['error', 'event', 'parent'],
    'no-shadow': 'warn',
    'no-shadow-restricted-names': 'error',
    'no-undef': 'error',
    'no-undef-init': 'warn',
    'no-undefined': 'off',
    'no-unused-vars': 'warn',
    'no-use-before-define': 'error',
};

// https://eslint.org/docs/rules/#node-js-and-commonjs
const nodeJsCommonJs = {
    'callback-return': [
        'warn',
        [
            "callback",
            "cb",
            "next"
        ]
    ],
    'global-require': 'error',
    'handle-callback-err': ['error', 'error'],
    'no-buffer-constructor': 'error',
    'no-mixed-requires': 'error',
    'no-new-require': 'error',
    'no-path-concat': 'error',
    'no-process-env': 'error',
    'no-process-exit': 'error',
    'no-restricted-modules': 'off',
    'no-sync': 'warn',
};

// https://eslint.org/docs/rules/#stylistic-issues
const stylisticIssues = {
    'array-bracket-newline': [
        'warn',
        {
            'multiline': true
        }
    ],
    'array-bracket-spacing': [
        'warn',
        'always',
        {
            'singleValue': true
        }
    ],
    'array-element-newline': ['warn', 'consistent'],
    'block-spacing': 'warn',
    'brace-style': ['warn', '1tbs'],
    'camelcase': 'warn',
    'capitalized-comments': 'off',
    'comma-dangle': ['warn', 'always-multiline'],
    'comma-spacing': 'warn',
    'comma-style': 'warn',
    'computed-property-spacing': ['warn', 'never'],
    'consistent-this': 'warn',
    'eol-last': ['warn', 'never'],
    'func-call-spacing': 'warn',
    'func-name-matching': 'error',
    'func-names': ['error', 'as-needed'],
    'func-style': 'warn',

    // Definition for rule 'function-call-argument-newline' was not found
    // 'function-call-argument-newline': [ 'warn', 'consistent' ],

    'function-paren-newline': ['warn', 'consistent'],
    'id-blacklist': ['warn'],
    'id-length': [
        'warn',
        {
            'min': 1,
            'max': 50,
            'properties': 'always',
            'exceptions': ['i', 'j']
        }
    ],
    'id-match': 'off',
    'implicit-arrow-linebreak': ['warn', 'beside'],
    'indent': ['warn', 4],
    'jsx-quotes': 'warn',
    'key-spacing': [
        'warn',
        {
            'mode': 'minimum'
        }
    ],
    'keyword-spacing': 'warn',
    'line-comment-position': [
        'warn',
        {
            'position': 'above'
        },
    ],
    'linebreak-style': ['warn', 'unix'],
    'lines-around-comment': [
        'warn',
        {
            'beforeBlockComment': true,
            'afterBlockComment': false,
            'beforeLineComment': false,
            'afterLineComment': false,
            'allowBlockStart': true,
            'allowBlockEnd': true,
            'allowObjectStart': true,
            'allowObjectEnd': false,
            'allowArrayStart': true,
            'allowArrayEnd': false,
            'allowClassStart': true,
            'allowClassEnd': false,
        },
    ],
    'lines-between-class-members': [
        'warn',
        'always',
        {
            'exceptAfterSingleLine': true
        }
    ],
    'max-depth': ['warn', 4],
    'max-len': [
        'warn',
        {
            'code': 160,
            'comments': 160
        }
    ],
    'max-lines': 'off',
    'max-lines-per-function': 'off',
    'max-nested-callbacks': [
        'warn',
        {
            'max': 10
        },
    ],
    'max-params': ['warn', 3],
    'max-statements': 'off',
    'max-statements-per-line': [
        'warn',
        {
            'max': 1,
        },
    ],
    'multiline-comment-style': ['warn', 'bare-block'],
    'multiline-ternary': ['warn', 'always-multiline'],
    'new-cap': 'warn',
    'new-parens': 'warn',
    'newline-per-chained-call': 'warn',
    'no-array-constructor': 'warn',
    'no-bitwise': 'warn',
    'no-continue': 'warn',
    'no-inline-comments': 'warn',
    'no-lonely-if': 'off',
    'no-mixed-operators': 'warn',
    'no-mixed-spaces-and-tabs': 'warn',
    'no-multi-assign': 'error',
    'no-multiple-empty-lines': [
        'warn',
        {
            'max': 3,
            'maxEOF': 0,
            'maxBOF': 0,
        },
    ],
    'no-negated-condition': 'warn',
    'no-nested-ternary': 'warn',
    'no-new-object': 'warn',
    'no-plusplus': 'off',

    // placeholder
    'no-restricted-syntax': 'warn',

    'no-tabs': 'error',
    'no-ternary': 'off',
    'no-trailing-spaces': 'warn',
    'no-underscore-dangle': 'error',
    'no-unneeded-ternary': 'warn',
    'no-whitespace-before-property': 'error',
    'nonblock-statement-body-position': ['warn', 'below'],
    'object-curly-newline': [
        'warn',
        {
            'multiline': true,
            'consistent': true,
        }
    ],
    'object-curly-spacing': ['warn', 'always'],
    'object-property-newline': [
        'warn',
        {
            'allowAllPropertiesOnSameLine': true,
        },
    ],
    'one-var': ['warn', 'never'],
    'one-var-declaration-per-line': 'warn',
    'operator-assignment': 'warn',
    'operator-linebreak': ['warn', 'before'],
    'padded-blocks': ['warn', 'never'],

    // placeholder
    'padding-line-between-statements': ['warn'],

    // Definition for rule 'prefer-exponentiation-operator' was not found
    // 'prefer-exponentiation-operator': 'warn',

    'prefer-object-spread': 'warn',
    'quote-props': [
        'warn',
        'consistent-as-needed',
        {
            'keywords': false,
        },
    ],
    'quotes': ['warn', 'single'],
    'semi': ['warn', 'always'],
    'semi-spacing': [
        'warn',
        {
            'before': false,
            'after': true,
        }
    ],
    'semi-style': ['warn', 'last'],
    'sort-keys': 'off',
    'sort-vars': 'off',
    'space-before-blocks': ['warn', 'always'],
    'space-before-function-paren': [
        'warn',
        {
            'anonymous': 'always',
            'named': 'never',
            'asyncArrow': 'always'
        },
    ],
    'space-in-parens': 'warn',
    'space-infix-ops': 'warn',
    'space-unary-ops': [
        'warn',
        {
            'words': true,
            'nonwords': false,
        },
    ],
    'spaced-comment': ['warn', 'always'],
    'switch-colon-spacing': 'warn',
    'template-tag-spacing': 'warn',
    'unicode-bom': 'warn',
    'wrap-regex': 'warn',
};

// https://eslint.org/docs/rules/#ecmascript-6
const ecmaScript6 = {
    'arrow-body-style': ['warn', 'always'],
    'arrow-parens': ['warn', 'always'],
    'arrow-spacing': 'warn',
    'constructor-super': 'warn',
    'generator-star-spacing': [
        'warn',
        {
            'before': true,
            'after': false,
        },
    ],
    'no-class-assign': 'error',
    'no-confusing-arrow': 'warn',
    'no-const-assign': 'error',
    'no-dupe-class-members': 'error',
    'no-duplicate-imports': 'warn',
    'no-new-symbol': 'error',

    // placeholder
    'no-restricted-imports': 'error',

    'no-this-before-super': 'error',
    'no-useless-computed-key': 'warn',
    'no-useless-constructor': 'warn',
    'no-useless-rename': 'warn',
    'no-var': 'error',
    'object-shorthand': 'warn',
    'prefer-arrow-callback': 'warn',
    'prefer-const': 'warn',
    'prefer-destructuring': 'off',
    'prefer-numeric-literals': 'warn',
    'prefer-rest-params': 'warn',
    'prefer-spread': 'warn',
    'prefer-template': 'warn',
    'require-yield': 'error',
    'rest-spread-spacing': ['warn', 'never'],
    'sort-imports': 'off',
    'symbol-description': 'warn',
    'template-curly-spacing': 'warn',
    'yield-star-spacing': 'warn',
};

// https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin#supported-rules
// @typescript-eslint
const typescriptRules = {
    '@typescript-eslint/adjacent-overload-signatures': 'warn',
    '@typescript-eslint/array-type': [
        'warn',
        {
            'default': 'generic',
        },
    ],

    // rule requires parserServices to be generated
    // '@typescript-eslint/await-thenable': 'warn',

    '@typescript-eslint/ban-ts-ignore': 'warn',
    '@typescript-eslint/ban-types': [
        'warn',
    ],

    // Definition for rule '@typescript-eslint/brace-style' was not found
    // 'brace-style': 'off',
    // '@typescript-eslint/brace-style': [ 'warn', '1tbs' ],

    'camelcase': 'off',
    '@typescript-eslint/camelcase': 'warn',

    '@typescript-eslint/class-name-casing': [
        'warn',
        {
            'allowUnderscorePrefix': true
        },
    ],

    // Definition for rule '@typescript-eslint/consistent-type-assertions' was not found
    // '@typescript-eslint/consistent-type-assertions': 'warn',

    '@typescript-eslint/consistent-type-definitions': ['warn', 'interface'],
    '@typescript-eslint/explicit-function-return-type': [
        'warn',
        {
            'allowExpressions': true,
        },
    ],
    '@typescript-eslint/explicit-member-accessibility': 'warn',

    'func-call-spacing': 'off',
    '@typescript-eslint/func-call-spacing': 'warn',

    '@typescript-eslint/generic-type-naming': 'off',

    'indent': 'off',
    '@typescript-eslint/indent': [
        'warn',
        4,
        {
            'SwitchCase': 1,
            'VariableDeclarator': 'first',
            'outerIIFEBody': 1,
            'MemberExpression': 1,
            'FunctionDeclaration': {
                'body': 1,
                'parameters': 1,
            },
            'FunctionExpression': {
                'body': 1,
                'parameters': 1,
            },
            'CallExpression': {
                'arguments': 1
            },
            'ArrayExpression': 1,
            'ObjectExpression': 1,
            'ImportDeclaration': 1,
            'flatTernaryExpressions': false,
            'ignoreComments': false,
        },
    ],

    '@typescript-eslint/interface-name-prefix': ['warn', 'always'],
    '@typescript-eslint/member-delimiter-style': 'warn',
    '@typescript-eslint/member-naming': 'off',
    '@typescript-eslint/member-ordering': 'off',

    'no-array-constructor': 'off',
    '@typescript-eslint/no-array-constructor': 'warn',

    // Definition for rule '@typescript-eslint/no-dynamic-delete' was not found
    // '@typescript-eslint/no-dynamic-delete': 'warn',

    '@typescript-eslint/no-empty-function': 'warn',
    '@typescript-eslint/no-empty-interface': 'warn',
    '@typescript-eslint/no-explicit-any': 'off',

    // Definition for rule '@typescript-eslint/no-extra-non-null-assertion' was not found
    // '@typescript-eslint/no-extra-non-null-assertion': 'warn',

    '@typescript-eslint/no-extra-parens': 'off',
    '@typescript-eslint/no-extraneous-class': [
        'warn',
        {
            'allowWithDecorator': true
        },
    ],

    // rule requires parserServices to be generated
    // '@typescript-eslint/no-floating-promises': 'warn',

    '@typescript-eslint/no-for-in-array': 'warn',
    '@typescript-eslint/no-inferrable-types': 'warn',

    'no-magic-numbers': 'off',
    '@typescript-eslint/no-magic-numbers': 'off',

    '@typescript-eslint/no-misused-new': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/no-misused-promises': 'warn',

    '@typescript-eslint/no-namespace': [
        'warn',
        {
            'allowDefinitionFiles': true,
        },
    ],
    '@typescript-eslint/no-non-null-assertion': 'warn',
    '@typescript-eslint/no-parameter-properties': 'warn',
    '@typescript-eslint/no-require-imports': 'warn',
    '@typescript-eslint/no-this-alias': [
        'warn',
        {
            'allowDestructuring': true,
            'allowedNames': ['that'],
        },
    ],
    '@typescript-eslint/no-type-alias': [
        'warn',
        {
            'allowAliases': 'in-unions-and-intersections',
            'allowCallbacks': 'always',
            'allowTupleTypes': 'always'
        },
    ],

    // Definition for rule '@typescript-eslint/no-unnecessary-condition' was not found
    // '@typescript-eslint/no-unnecessary-condition': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/no-unnecessary-qualifier': 'warn',

    // Definition for rule '@typescript-eslint/no-unnecessary-type-arguments' was not found
    // '@typescript-eslint/no-unnecessary-type-arguments': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/no-unnecessary-type-assertion': 'warn',

    // Definition for rule '@typescript-eslint/no-untyped-public-signature' was not found
    // '@typescript-eslint/no-untyped-public-signature': 'warn',

    // Definition for rule '@typescript-eslint/no-unused-expressions' was not found
    // '@typescript-eslint/no-unused-expressions': 'warn',

    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': 'warn',

    // Definition for rule '@typescript-eslint/no-unused-vars-experimental' was not found
    // '@typescript-eslint/no-unused-vars-experimental': 'warn',

    '@typescript-eslint/no-use-before-define': [
        'warn',
        {
            'functions': true,
            'classes': true,
            'variables': true,
            'typedefs': true,
        },
    ],
    '@typescript-eslint/no-useless-constructor': 'warn',
    '@typescript-eslint/no-var-requires': 'warn',
    '@typescript-eslint/prefer-for-of': 'warn',
    '@typescript-eslint/prefer-function-type': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/prefer-includes': 'warn',

    '@typescript-eslint/prefer-namespace-keyword': 'warn',

    // Definition for rule '@typescript-eslint/prefer-nullish-coalescing' was not found
    // '@typescript-eslint/prefer-nullish-coalescing': 'warn',

    // Definition for rule '@typescript-eslint/prefer-optional-chain' was not found
    // '@typescript-eslint/prefer-optional-chain': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/prefer-readonly': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/prefer-regexp-exec': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/prefer-string-starts-ends-with': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/promise-function-async': 'warn',

    // Definition for rule '@typescript-eslint/quotes' was not found
    // '@typescript-eslint/quotes': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/require-array-sort-compare': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/require-await': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/restrict-plus-operands': 'warn',

    // Definition for rule '@typescript-eslint/restrict-template-expressions' was not found
    // '@typescript-eslint/restrict-template-expressions': 'warn',

    // Definition for rule '@typescript-eslint/return-await' was not found
    // '@typescript-eslint/return-await': 'warn',

    'semi': 'off',
    '@typescript-eslint/semi': 'warn',

    // Definition for rule '@typescript-eslint/space-before-function-paren' was not found
    // '@typescript-eslint/space-before-function-paren': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/strict-boolean-expressions': 'warn',

    '@typescript-eslint/triple-slash-reference': [
        'warn',
        {
            'path': 'never',
            'types': 'never',
            'lib': 'never',
        },
    ],
    '@typescript-eslint/type-annotation-spacing': 'warn',

    // Definition for rule '@typescript-eslint/typedef' was not found
    // '@typescript-eslint/typedef': 'warn',

    // rule requires parserServices to be generated
    // '@typescript-eslint/unbound-method': 'warn',

    '@typescript-eslint/unified-signatures': 'warn',
};

const ruleSections = [possibleErrors, bestPractices, variables, nodeJsCommonJs, stylisticIssues, ecmaScript6, typescriptRules];

const rules = {};
ruleSections.forEach((section) => {
    Object.getOwnPropertyNames(section).forEach((name) => {
        rules[name] = section[name];
    })
});


module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: [
        'plugin:vue/essential',
        '@vue/standard',
        '@vue/typescript/recommended'
    ],
    parserOptions: {
        ecmaVersion: 2020
    },
    rules
};