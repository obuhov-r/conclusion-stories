/* eslint-disable no-process-env */

module.exports = {
    devServer: {
        port: 10020,
    },
    publicPath: process.env.NODE_ENV === 'production'
        ? `/${process.env.CI_PROJECT_NAME}/`
        : '/',
};